<?php

/**
 * Load assets for our blocks.
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Load general assets for our blocks.
 *
 * @since 4.4.0
 */
class GutenbergBlocks_Render_Block
{
    /**
     * This plugin's instance.
     *
     * @var GutenbergBlocks_Render_Block
     */
    private static $instance;

    /**
     * Registers the plugin.
     */
    public static function register()
    {
        if (null === self::$instance) {
            self::$instance = new GutenbergBlocks_Render_Block();
        }
    }

    /**
     * The block attributes.
     *
     * @var string $_attributes
     */
    private $attributes;

    /**
     * The Constructor.
     */
    private function __construct()
    {
        if (!is_admin()) {
            add_action('render_block', array($this, 'render_block'), 5, 2);
        }
    }

    /**
     * Set block attributes.
     *
     * @param array $block The block data.
     *
     * @return array Returns the block attributes.
     */
    private function block_attributes($block)
    {
        if (isset($block['attrs']) && isset($block['attrs']['mediasoep']) && is_array($block['attrs'])) {

            return $block['attrs']['mediasoep'];
        }

        return [];
    }

    /**
     * Set visibility based on custom display logic.
     *
     * @param mixed $block_content The block content.
     *
     * @return mixed Returns the new block content.
     */
    private function display_logic($block_content)
    {
        if (isset($this->attributes['visibilityLogic']) && !empty($this->attributes['visibilityLogic'])) {
            // Do display logic.
            $logic = stripslashes(trim($this->attributes['visibilityLogic']));

            // Allow override filters.
            $logic = apply_filters('gutenberg_blocks/attributes/visibility_logic', $logic);
            if (true === $logic) {
                return '';
            }

            if (false === $logic) {
                return $block_content;
            }

            if (stristr($logic, 'return') === false) {
                $logic = 'return (' . html_entity_decode($logic, ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . ');';
            }

            // Ignore phpcs since warning is available as description on this feature.
            if (eval($logic)) { // phpcs:ignore
                return '';
            }
        }

        return $block_content;
    }

    /**
     * Render block.
     *
     * @param mixed $block_content The block content.
     * @param array $block The block data.
     *
     * @return mixed Returns the new block content.
     */
    public function render_block($block_content, $block)
    {
        $this->attributes = $this->block_attributes($block);

        $block_content = $this->display_logic($block_content);

        return $block_content;
    }
}

GutenbergBlocks_Render_Block::register();
