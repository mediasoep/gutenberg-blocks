/**
 * WordPress dependencies
 */
const { useEffect } = wp.element;
const { createHigherOrderComponent } = wp.compose;

const withUniqueId = createHigherOrderComponent(
	WrappedComponent => props => {
		const { clientId, attributes, setAttributes } = props;
		const { uniqueID } = attributes;

		// Unique ID
		useEffect( () => {
			const unique = clientId.substr( 0, 7 );
			if ( typeof uniqueID === 'undefined' || uniqueID !== unique ) {
				setAttributes( { uniqueID: unique } );
			}
		}, [] );

		return (
			<WrappedComponent { ...props } />
		);
	},
	'withUniqueId'
);

export default withUniqueId;
