/* eslint-disable jsx-a11y/anchor-is-valid */
/**
 * BLOCK: MS Gutenberg Blocks - Responsive Accordion Tabs
 */

/**
 * External dependencies
 */
import classnames from 'classnames';
import times from 'lodash/times';
import memoize from 'memize';

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Component, Fragment } = wp.element;
const { compose } = wp.compose;
const { withSelect, withDispatch } = wp.data;
const {	createBlock } = wp.blocks;
const { InnerBlocks, InspectorControls, RichText } = wp.blockEditor;
const {
	BaseControl,
	Button,
	ButtonGroup,
	Dashicon,
	PanelBody,
	RangeControl,
	ToggleControl,
} = wp.components;

/**
 * A list of blocks that are allowed in this block.
 */
const ALLOWED_BLOCKS = [ 'ms/panel' ];

/**
 * This allows for checking to see if the block needs to generate a new ID.
 */
const uniqueIDs = [];

/**
 * Returns the layouts configuration for a given number of panels.
 *
 * @param {number} panels Number of panes.
 *
 * @return {Object[]} Panels layout configuration.
 */
const getPanelsTemplate = memoize( ( panels ) => {
	return times( panels, n => [ 'ms/panel', { index: n + 1 } ] );
} );

class ResponsiveAccordionTabsEdit extends Component {
	constructor( props ) {
		super( props );
		this.getTabs = this.getTabs.bind( this );
	}

	componentDidMount() {
		if ( ! this.props.attributes.uniqueID ) {
			this.props.setAttributes( {
				uniqueID: this.props.clientId.substr( 2, 9 ),
			} );
			uniqueIDs.push( this.props.clientId.substr( 2, 9 ) );
		} else if ( uniqueIDs.includes( this.props.attributes.uniqueID ) ) {
			this.props.attributes.uniqueID = this.props.clientId.substr( 2, 9 );
			this.props.setAttributes( {
				uniqueID: this.props.clientId.substr( 2, 9 ),
			} );
			uniqueIDs.push( this.props.clientId.substr( 2, 9 ) );
		} else {
			uniqueIDs.push( this.props.attributes.uniqueID );
		}
	}

	getTabs() {
		return this.props.block.innerBlocks;
	}

	render() {
		const {
			block,
			className,
			setAttributes,
			isSelected,
			updateBlockAttributes,
			removeBlock,
			selectedBlock,
			selectBlock,
			insertBlock,
			clientId,
			attributes: {
				uniqueID,
				activeControl,
				activePanel,
				tabsTitle,
				desktopType,
				tabletType,
				mobileType,
				accordionInitiallyClosePanes,
				accordionAllowAllClosed,
				accordionMultiExpand,
				topMargin,
				bottomMargin,
				tabletScalingFactor,
				mobileScalingFactor,
			},
		} = this.props;

		const updateTimeStamp = () => {
			setAttributes( { timestamp: new Date().getTime() } );
		};

		const showControls = ( type, index ) => {
			// Set the active tab to know which one is selected.
			setAttributes( { activeControl: type + '-' + index } );
			setAttributes( { activePanel: index } );

			const tabs = this.getTabs();

			// Update each tab's active state.
			tabs.forEach( ( tab, i ) => {
				updateBlockAttributes( tab.clientId, {
					index: i,
					isActive: index === i,
				} );
			} );
		};

		const onChangeTabTitle = ( content, i ) => {
			// Save the new tab title.
			tabsTitle[ i ] = { content };
			setAttributes( { tabsTitle: tabsTitle } );

			// Update the timestamp.
			updateTimeStamp();
		};

		const addTab = i => {
			// Set a default tab title.
			tabsTitle[ i ] = { content: 'Tab' };
			setAttributes( { tabsTitle: tabsTitle } );

			// Make the new tab active.
			setAttributes( { activePanel: i } );
			showControls( 'tab-title', i );

			const newBlock = createBlock( 'ms/panel', { index: i } );
			insertBlock( newBlock, parseInt( block.innerBlocks.length ), clientId );

			// Update the timestamp.
			updateTimeStamp();
		};

		const removeTab = i => {
			// Remove the selected tab (index) from the array of titles.
			setAttributes( {
				tabsTitle: tabsTitle.filter( ( title, idx ) => idx !== i ),
			} );

			// Get the associated tab block by index, and remove it.
			removeBlock( tabs.filter( tab => tab.attributes.index === i )[ 0 ].clientId ).then( () => {
				this.props.resetOrder();
				showControls( 'tab-title', 0 );
			} );

			// Update the timestamp.
			updateTimeStamp();
		};

		const tabs = this.getTabs();

		if ( selectedBlock && selectedBlock.clientId !== this.props.block.clientId ) {
			if (
				tabs.filter( innerBlock => innerBlock.attributes.isActive ).length === 0
			) {
				showControls( 'tab-title', tabs.length - 1 );
			}
			if (
				tabs.filter( tab => tab.clientId === selectedBlock.clientId ).length >
					0 &&
				! selectedBlock.attributes.isActive
			) {
				selectBlock( this.props.block.clientId );
			}
		}

		const classes = classnames( 'ms_responsive-accordion-tabs', className );

		const spacingStyles = {
			'margin-top': topMargin !== 0 ? topMargin : undefined,
			'margin-bottom': bottomMargin !== 0 ? bottomMargin : undefined,
		};

		// Remove undefined spacings.
		Object.keys( spacingStyles ).forEach( key =>
			spacingStyles[ key ] === undefined && delete spacingStyles[ key ]
		);

		const panelStyles = [];
		let inlineStyles = null;

		tabs.forEach( tab => {
			const { index, topPadding, bottomPadding, leftPadding, rightPadding, tabletScalingFactor: tabTabletScalingFactor, mobileScalingFactor: tabMobileScalingFactor } = tab.attributes;
			const tabSpacingStyles = {
				'padding-top': topPadding !== 0 ? topPadding : undefined,
				'padding-bottom': bottomPadding !== 0 ? bottomPadding : undefined,
				'padding-left': leftPadding !== 0 ? leftPadding : undefined,
				'padding-right': rightPadding !== 0 ? rightPadding : undefined,
			};

			// Remove undefined spacings.
			Object.keys( tabSpacingStyles ).forEach( key =>
				tabSpacingStyles[ key ] === undefined && delete tabSpacingStyles[ key ]
			);

			if ( tabSpacingStyles !== undefined && Object.entries( tabSpacingStyles ).length !== 0 ) {
				let mobile = `\t#block_${ uniqueID } #panel${ index } {\n`;
				let tablet = `\t#block_${ uniqueID } #panel${ index } {\n`;
				let desktop = `\t#block_${ uniqueID } #panel${ index } {\n`;

				for ( const style in tabSpacingStyles ) {
					mobile += `\t\t${ style }: ${ tabSpacingStyles[ style ] *
						( ( tabMobileScalingFactor || 50 ) / 100 ) }rem;\n`;
					tablet += `\t\t${ style }: ${ tabSpacingStyles[ style ] *
						( ( tabTabletScalingFactor || 100 ) / 100 ) }rem;\n`;
					desktop += `\t\t${ style }: ${ tabSpacingStyles[ style ] }rem;\n`;
				}

				mobile += '\t}';
				tablet += '\t}';
				desktop += '\t}';

				panelStyles.push( {
					mobile: mobile,
					tablet: tablet,
					desktop: desktop,
				} );
			}
		} );

		if ( spacingStyles !== undefined && Object.entries( spacingStyles ).length !== 0 ) {
			const keys = Object.keys( spacingStyles );
			const last = keys[ keys.length - 1 ];

			let mobile = `@media screen and (max-width: 39.9375em) {\n\t#block_${ uniqueID } {\n`;
			let tablet = `@media screen and (min-width: 40em) and (max-width: 63.9375em) {\n\t#block_${ uniqueID } {\n`;
			let desktop = `@media screen and (min-width: 64em) {\n\t#block_${ uniqueID } {\n`;

			for ( const style in spacingStyles ) {
				mobile += `\t\t${ style }: ${ spacingStyles[ style ] *
					( ( mobileScalingFactor || 50 ) / 100 ) }rem;`;
				tablet += `\t\t${ style }: ${ spacingStyles[ style ] *
					( ( tabletScalingFactor || 100 ) / 100 ) }rem;`;
				desktop += `\t\t${ style }: ${ spacingStyles[ style ] }rem;`;
				if ( last !== style ) {
					mobile += '\n';
					tablet += '\n';
					desktop += '\n';
				}
			}

			mobile += '\n\t}';
			tablet += '\n\t}';
			desktop += '\n\t}';

			if ( panelStyles.length > 0 ) {
				mobile += '\n\n' + panelStyles.map( e => e.mobile ).join( '\n\n' );
				tablet += '\n\n' + panelStyles.map( e => e.tablet ).join( '\n\n' );
				desktop += '\n\n' + panelStyles.map( e => e.desktop ).join( '\n\n' );
			}

			// Media query end brace.
			mobile += '\n}';
			tablet += '\n}';
			desktop += '\n}';

			inlineStyles = `${ mobile }\n\n${ tablet }\n\n${ desktop }`;
		} else if ( panelStyles.length > 0 ) {
			let mobile = '@media screen and (max-width: 39.9375em) {\n';
			let tablet = '@media screen and (min-width: 40em) and (max-width: 63.9375em) {\n';
			let desktop = '@media screen and (min-width: 64em) {\n';

			mobile += panelStyles.map( e => e.mobile ).join( '\n\n' );
			tablet += panelStyles.map( e => e.tablet ).join( '\n\n' );
			desktop += panelStyles.map( e => e.desktop ).join( '\n\n' );

			// Media query end brace.
			mobile += '\n}';
			tablet += '\n}';
			desktop += '\n}';

			inlineStyles = `${ mobile }\n\n${ tablet }\n\n${ desktop }`;
		}

		setAttributes( { styles: inlineStyles } );

		const responsiveAccordionTabs = classnames( {
			[ mobileType ]: mobileType,
			[ `medium-${ tabletType }` ]: tabletType,
			[ `large-${ desktopType }` ]: desktopType,
		} );

		const accordionOptions = [
			desktopType,
			tabletType,
			mobileType,
		].some( el => el === 'accordion' ) ?
			{
				'data-allow-all-closed': accordionAllowAllClosed,
				'data-multi-expand': accordionMultiExpand,
			} :
			{};

		return (
			<Fragment>
				<InspectorControls>
					<PanelBody
						title={ __( 'Responsive Accordion Tabs', 'ms-gutenberg-blocks' ) }
						initialOpen={ true }
					>
						<BaseControl label={ __( 'Desktop', 'ms-gutenberg-blocks' ) }>
							<ButtonGroup aria-label={ __( 'Desktop', 'ms-gutenberg-blocks' ) }>
								<Button
									isPrimary={ desktopType === 'accordion' }
									isSecondary={ desktopType !== 'accordion' }
									onClick={ () => {
										setAttributes( {
											desktopType: 'accordion',
										} );
									} }
								>
									{ __( 'Accordion', 'ms-gutenberg-blocks' ) }
								</Button>
								<Button
									isPrimary={ desktopType === 'tabs' }
									isSecondary={ desktopType !== 'tabs' }
									onClick={ () => {
										setAttributes( {
											desktopType: 'tabs',
										} );
									} }
								>
									{ __( 'Tabs', 'ms-gutenberg-blocks' ) }
								</Button>
							</ButtonGroup>
						</BaseControl>
						<BaseControl label={ __( 'Tablet', 'ms-gutenberg-blocks' ) }>
							<ButtonGroup aria-label={ __( 'Tablet', 'ms-gutenberg-blocks' ) }>
								<Button
									isPrimary={ tabletType === 'accordion' }
									isSecondary={ tabletType !== 'accordion' }
									onClick={ () => {
										setAttributes( {
											tabletType: 'accordion',
										} );
									} }
								>
									{ __( 'Accordion', 'ms-gutenberg-blocks' ) }
								</Button>
								<Button
									isPrimary={ tabletType === 'tabs' }
									isSecondary={ tabletType !== 'tabs' }
									onClick={ () => {
										setAttributes( {
											tabletType: 'tabs',
										} );
									} }
								>
									{ __( 'Tabs', 'ms-gutenberg-blocks' ) }
								</Button>
							</ButtonGroup>
						</BaseControl>
						<BaseControl label={ __( 'Mobile', 'ms-gutenberg-blocks' ) }>
							<ButtonGroup aria-label={ __( 'Mobile', 'ms-gutenberg-blocks' ) }>
								<Button
									isPrimary={ mobileType === 'accordion' }
									isSecondary={ mobileType !== 'accordion' }
									onClick={ () => {
										setAttributes( {
											mobileType: 'accordion',
										} );
									} }
								>
									{ __( 'Accordion', 'ms-gutenberg-blocks' ) }
								</Button>
								<Button
									isPrimary={ mobileType === 'tabs' }
									isSecondary={ mobileType !== 'tabs' }
									onClick={ () => {
										setAttributes( {
											mobileType: 'tabs',
										} );
									} }
								>
									{ __( 'Tabs', 'ms-gutenberg-blocks' ) }
								</Button>
							</ButtonGroup>
						</BaseControl>
					</PanelBody>
					{ [
						desktopType,
						tabletType,
						mobileType,
					].some( el => el === 'accordion' ) && (
						<PanelBody
							title={ __( 'Accordion settings', 'ms-gutenberg-blocks' ) }
							initialOpen={ true }
						>
							<ToggleControl
								label={ __( 'Initially close all panes', 'ms-gutenberg-blocks' ) }
								checked={ accordionInitiallyClosePanes }
								onChange={ () =>
									setAttributes( {
										accordionInitiallyClosePanes: ! accordionInitiallyClosePanes,
									} )
								}
							/>
							<ToggleControl
								label={ __( 'Allow all panes closed', 'ms-gutenberg-blocks' ) }
								checked={ accordionAllowAllClosed }
								onChange={ () =>
									setAttributes( {
										accordionAllowAllClosed: ! accordionAllowAllClosed,
									} )
								}
							/>
							<ToggleControl
								label={ __( 'Allow multiple panes opened', 'ms-gutenberg-blocks' ) }
								checked={ accordionMultiExpand }
								onChange={ () =>
									setAttributes( {
										accordionMultiExpand: ! accordionMultiExpand,
									} )
								}
							/>
						</PanelBody>
					) }
					<PanelBody
						title={ __( 'Margin', 'ms-gutenberg-blocks' ) }
						initialOpen={ true }
					>
						<h2>{ __( 'Margin (rem)', 'ms-gutenberg-blocks' ) }</h2>
						<RangeControl
							label={ __( 'Top', 'ms-gutenberg-blocks' ) }
							value={ topMargin }
							onChange={ value => {
								setAttributes( {
									topMargin: value,
								} );
							} }
							min={ -20 }
							max={ 20 }
						/>
						<RangeControl
							label={ __( 'Bottom', 'ms-gutenberg-blocks' ) }
							value={ bottomMargin }
							onChange={ value => {
								setAttributes( {
									bottomMargin: value,
								} );
							} }
							min={ -20 }
							max={ 20 }
						/>
						<h2>{ __( 'Scaling on tablet and mobile', 'ms-gutenberg-blocks' ) }</h2>
						<p>
							<i>
								{ __(
									'Percentage based on the values selected in margin and padding.',
									'ms-gutenberg-blocks'
								) }
							</i>
						</p>
						<RangeControl
							label={ __( 'Tablet scaling factor (%)', 'ms-gutenberg-blocks' ) }
							value={ tabletScalingFactor }
							onChange={ value => {
								setAttributes( {
									tabletScalingFactor: value,
								} );
							} }
							min={ 5 }
							max={ 100 }
							step={ 5 }
						/>
						<RangeControl
							label={ __( 'Mobile scaling factor (%)', 'ms-gutenberg-blocks' ) }
							value={ mobileScalingFactor }
							onChange={ value => {
								setAttributes( {
									mobileScalingFactor: value,
								} );
							} }
							min={ 5 }
							max={ 100 }
							step={ 5 }
						/>
					</PanelBody>
				</InspectorControls>
				<div
					id={ `block_${ uniqueID }` }
					className={ classes }
				>
					<ul
						id={ uniqueID }
						className="tabs"
						data-responsive-accordion-tabs={ responsiveAccordionTabs }
						{ ...accordionOptions }
					>
						{ tabsTitle.map( ( value, index ) => (
							// eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
							<li
								key={ index }
								className={ classnames( 'tabs-title', {
									'is-active': activePanel === index,
								} ) }
								onClick={ () => showControls( 'tab-title', index ) }
								onKeyDown={ () => showControls( 'tab-title', index ) }
							>
								<RichText
									tagName="a"
									className="tabs-title-a"
									value={ value.content }
									allowedFormats={ [ 'bold', 'italic' ] }
									isSelected={
										activeControl === 'tab-title-' + index &&
										isSelected
									}
									onChange={ content => onChangeTabTitle( content, index ) }
									placeholder="Tab"
								/>
								{ tabs.length > 1 &&
									<Dashicon icon="no" className="tabs-remove-tab" onClick={ () => removeTab( index ) } />
								}
							</li>
						) ) }
						<li
							key={ tabsTitle.length }
							className={ 'tabs-title tabs-add-new' }
						>
							<a
								role="button"
								tabIndex="0"
								onClick={ () => addTab( tabsTitle.length ) }
								onKeyDown={ () => addTab( tabsTitle.length ) }
							>
								<Dashicon icon="plus" />
							</a>
						</li>
					</ul>
					<div className="tabs-content" data-tabs-content={ uniqueID }>
						<InnerBlocks
							template={ getPanelsTemplate( tabsTitle.length ) }
							templateLock={ false }
							allowedBlocks={ ALLOWED_BLOCKS }
						/>
					</div>
					{ inlineStyles &&
						<style
							dangerouslySetInnerHTML={ {
								__html: inlineStyles,
							} }
						/>
					}
				</div>
			</Fragment>
		);
	}
}

export default compose( [
	withSelect( ( select, ownProps ) => {
		const { getBlock, getSelectedBlock } = select( 'core/block-editor' );
		const { clientId } = ownProps;

		return {
			clientId,
			block: getBlock( clientId ),
			selectedBlock: getSelectedBlock(),
		};
	} ),
	withDispatch( ( dispatch, { clientId }, { select } ) => {
		const {
			getBlock,
		} = select( 'core/block-editor' );

		const {
			updateBlockAttributes,
			insertBlock,
			insertBlocks,
			removeBlock,
			moveBlockToPosition,
			selectBlock,
		} = dispatch( 'core/block-editor' );

		const block = getBlock( clientId );

		return {
			resetOrder() {
				times( block.innerBlocks.length, n => {
					updateBlockAttributes( block.innerBlocks[ n ].clientId, {
						id: n,
					} );
				} );
			},
			updateBlockAttributes,
			insertBlock,
			insertBlocks,
			removeBlock,
			moveBlockToPosition,
			selectBlock,
		};
	} ),
] )( ResponsiveAccordionTabsEdit );
