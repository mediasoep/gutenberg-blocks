/**
 * BLOCK: MS Gutenberg Blocks - Responsive Accordion Tabs
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Path, SVG } = wp.components;

/**
 * Internal dependencies
 */
import attributes from './attributes';
import edit from './edit';
import save from './save';
// import transforms from './transforms';
import deprecated from './deprecated';

//  Import CSS.
import './style.scss';
import './editor.scss';

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'ms/responsive-accordion-tabs', {
	title: __( 'Responsive Accordion Tabs', 'ms-gutenberg-blocks' ),
	icon: ( <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" width="24" height="24">
		<Path fill="#f29f95" d="M69.08 271.63L0 390.05V112a48 48 0 0 1 48-48h160l64 64h160a48 48 0 0 1 48 48v48H152a96.31 96.31 0 0 0-82.92 47.63z" /><Path d="M152 256h400a24 24 0 0 1 20.73 36.09l-72.46 124.16A64 64 0 0 1 445 448H45a24 24 0 0 1-20.73-36.09l72.45-124.16A64 64 0 0 1 152 256z" fill="#e84f3d" />
	</SVG>	),
	category: 'common',
	keywords: [ __( 'Responsive Accordion Tabs', 'ms-gutenberg-blocks' ), 'mediasoep' ],
	supports: {
		align: [ 'wide', 'full' ],
	},
	attributes,
	edit,
	save,
	deprecated,
} );
