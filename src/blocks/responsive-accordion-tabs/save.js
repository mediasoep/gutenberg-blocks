/**
 * BLOCK: MS Gutenberg Blocks - Responsive Accordion Tabs
 */

/**
 * External dependencies
 */
import classnames from 'classnames';

/**
 * WordPress dependencies
 */
const { InnerBlocks, RichText } = wp.blockEditor;

export default function save( { attributes, className } ) {
	const {
		styles,
		uniqueID,
		activePanel,
		tabsTitle,
		desktopType,
		tabletType,
		mobileType,
		accordionInitiallyClosePanes,
		accordionAllowAllClosed,
		accordionMultiExpand,
	} = attributes;

	const classes = classnames( 'ms-responsive-accordion-tabs', className );

	const responsiveAccordionTabs = classnames( {
		[ mobileType ]: mobileType,
		[ `medium-${ tabletType }` ]: tabletType,
		[ `large-${ desktopType }` ]: desktopType,
	} );

	const accordionBreakpoints = [
		desktopType,
		tabletType,
		mobileType,
	];

	const accordionOptions = accordionBreakpoints.some(
		( el ) => el === 'accordion'
	) ?
		{
			'data-allow-all-closed': accordionAllowAllClosed,
			'data-multi-expand': accordionMultiExpand,
		} :
		{};

	let shouldAccordionInitiallyClosePanes = false;
	if ( accordionBreakpoints ) {
		shouldAccordionInitiallyClosePanes = accordionBreakpoints.every( ( el ) => el === 'accordion' ) ?
			accordionInitiallyClosePanes :
			shouldAccordionInitiallyClosePanes;
	}

	return (
		<div id={ `block_${ uniqueID }` } className={ classes }>
			<ul
				id={ uniqueID }
				className="tabs"
				data-responsive-accordion-tabs={ responsiveAccordionTabs }
				{ ...accordionOptions }
			>
				{ tabsTitle.map( ( value, i ) => {
					return (
						<li
							key={ i }
							className={ classnames( 'tabs-title', {
								'is-active':
									activePanel === i && ! shouldAccordionInitiallyClosePanes,
							} ) }
						>
							<RichText.Content
								tagName="a"
								className="tabs-title-a"
								href={ `#panel${ i }` }
								value={ value.content }
							/>
						</li>
					);
				} ) }
			</ul>
			<div className="tabs-content" data-tabs-content={ uniqueID }>
				<InnerBlocks.Content />
			</div>
			{ styles && (
				<style
					dangerouslySetInnerHTML={ {
						__html: styles,
					} }
				/>
			) }
		</div>
	);
}
