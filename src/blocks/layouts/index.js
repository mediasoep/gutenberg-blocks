/**
 * BLOCK: MS Gutenberg Blocks - Layouts
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { createBlock, registerBlockType } = wp.blocks;
const { Button, Path, SVG } = wp.components;
const { dispatch, subscribe } = wp.data;
const { render } = wp.element;
const { domReady } = wp;

/**
 * Internal dependencies
 */
import edit from './edit';

//  Import CSS.
// import './style.scss';
import './editor.scss';

/**
 * Layout icon.
 */
const layoutIcon = () => {
	return ( <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="24" height="24">
		<Path fill="#f29f95" d="M192 224V416H128V224H64V160H448V224H192Z" /><Path d="M448 32H64C28.654 32 0 60.652 0 96V416C0 451.346 28.654 480 64 480H448C483.346 480 512 451.346 512 416V96C512 60.652 483.346 32 448 32ZM448 416H64V96H448V416Z" fill="#e84f3d" />
	</SVG> );
};

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'ms/layouts', {
	title: __( 'Layouts', 'ms-gutenberg-blocks' ),
	icon: layoutIcon,
	category: 'layout',
	keywords: [ __( 'Layouts', 'ms-gutenberg-blocks' ), 'mediasoep' ],
	edit,
	save: () => null,
} );

/**
 * Build the layout inserter button.
 */
const InsertLibraryButton = () => {
	return (
		<Button
			isSecondary
			onClick={ () => {
				// Insert a layout library block.
				const block = createBlock( 'ms/layouts' );
				dispatch( 'core/block-editor' ).insertBlocks( block );
			} }
			id="ms-layout-insert-button"
			label={ __( 'Insert Layout', 'ms-gutenberg-blocks' ) }
			icon={ layoutIcon }
		>
			{ __( 'Layouts', 'ms-gutenberg-blocks' ) }
		</Button>
	);
};

/**
 * Add a MS Layout button to the toolbar.
 */
const mountLayoutLibrary = () => {
	let timeout = null;
	const unsubscribe = subscribe( () => {
		const toolbar = document.querySelector( '.edit-post-header-toolbar' );
		if ( ! toolbar ) {
			return;
		}

		const buttonDiv = document.createElement( 'div' );
		buttonDiv.classList.add( 'ms-layout-button-wrapper' );

		if ( ! toolbar.querySelector( '.ms-layout-button-wrapper' ) ) {
			render( <InsertLibraryButton />, buttonDiv );
			toolbar.appendChild( buttonDiv );
		}

		if ( timeout ) {
			clearTimeout( timeout );
		}

		timeout = setTimeout( () => {
			if ( document.querySelector( '.ms-layout-button-wrapper' ) ) {
				unsubscribe();
			}
		}, 0 );
	} );
};

domReady( mountLayoutLibrary );
