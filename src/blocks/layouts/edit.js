/* eslint-disable no-undef */
/**
 * BLOCK: MS Gutenberg Blocks - Layouts
 */

/**
 * External dependencies
 */
import { Row, Col } from 'react-grid-system';

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { compose } = wp.compose;
const { rawHandler } = wp.blocks;
const { withSelect, withDispatch } = wp.data;
const { Fragment, useState, useEffect, useMemo } = wp.element;
const {
	Button,
	ButtonGroup,
	Card,
	CardBody,
	CardFooter,
	CardMedia,
	Modal,
	Placeholder,
	SelectControl,
	Spinner,
} = wp.components;

const edit = ( { replaceBlocks, removeBlock, clientId, layoutPosts } ) => {
	const [ isOpen, setOpen ] = useState( false );
	const openModal = () => setOpen( true );
	const closeModal = () => setOpen( false );

	const [ layouts, setLayouts ] = useState( [] );
	const [ categories, setCategories ] = useState( [ 'all' ] );
	const [ activeCategory, setActiveCategory ] = useState( 'all' );

	const loadingPosts = layoutPosts === null ? true : false;
	const hasPosts = Array.isArray( layoutPosts ) && layoutPosts.length;

	// Open the modal.
	useEffect( () => {
		openModal();
	}, [] );

	// Retrieve available layouts.
	useEffect( () => {
		if ( hasPosts ) {
			const posts = layoutPosts.map( post => {
				return {
					name: post.title.rendered,
					description: post.excerpt.rendered,
					category: post.category_names.length > 0 ? post.category_names : [ 'uncategorized' ],
					content: post.content.raw,
					image: ( post.featured_image_urls && post.featured_image_urls.large ) ? post.featured_image_urls.large[ 0 ] : `https://dummyimage.com/400x600/000/fff&text=${ post.title.rendered }`,
				};
			} );

			setLayouts( prev => [ ...prev, ...posts ] );
		}
	}, [ layoutPosts ] );

	// Get non duplicate categories from layouts.
	useEffect( () => {
		for ( const layout of layouts ) {
			for ( const name of layout.category ) {
				if ( ! categories.includes( name ) ) {
					setCategories( prev => [ ...prev, name ] );
				}
			}
		}
	}, [ layouts, categories ] );

	// Category options for select.
	const categoryOptions = useMemo( () => categories.map( ( item ) => {
		return {
			value: item, label: item.charAt( 0 ).toUpperCase() + item.slice( 1 ) };
	} ), [ categories ] );

	// Filter layouts based on selected category.
	const renderedLayouts = layouts.map( ( { name, description, category, content, image } ) => {
		if ( activeCategory === 'all' || category.includes( activeCategory ) ) {
			return (
				<Col sm={ 6 } key={ name }>
					<Card isElevated size="small">
						<CardMedia className={ 'image-10:15' }>
							<img
								src={ image }
								alt={ name }
							/>
						</CardMedia>
						<CardBody>
							<h2>{ name }</h2>
							{ description &&
								<p dangerouslySetInnerHTML={ { __html: description } }></p>
							}
						</CardBody>
						<CardFooter style={ { textAlign: 'right' } }>
							<Button isPrimary onClick={ () => {
								replaceBlocks( content );
							} }>
								{ __( 'Add Layout', 'ms-gutenberg-blocks' ) }
							</Button>
						</CardFooter>
					</Card>
				</Col>
			);
		}
	} );

	return (
		<Fragment>
			<Placeholder
				key="placeholder"
				label={ __( 'Layout Selector', 'ms-gutenberg-blocks' ) }
				instructions={ __( 'Launch the layout library to browse pre-designed sections.', 'ms-gutenberg-blocks' ) }
				className={ 'ms-layout-selector-placeholder' }
				icon="layout"
			>
				<Button isSecondary className="ms-layout-selector-button" onClick={ openModal }>Open Modal</Button>
				{ isOpen && (
					<Modal
						title={ __( 'Choose a layout', 'ms-gutenberg-blocks' ) }
						onRequestClose={ closeModal }>
						<Row>
							{ /* Show spinner when loading. */ }
							{ loadingPosts &&
								<Col sm={ 12 } className={ 'ms-loading-indicator' }>
									<Spinner />
								</Col>
							}

							{ /* Show message to add new layouts when there are none. */ }
							{ ( ! loadingPosts && ! hasPosts ) &&
								<Col sm={ 12 }>
									{ __( 'There are no layouts available! Create some at the layouts post type. Once you have created a new layout, refresh this page to load it here.', 'ms-gutenberg-block' ) }
									<br />
									<br />
									<ButtonGroup>
										<Button isPrimary href={ localizedBlocks.layoutsPostTypePage } target="_blank">
											{ __( 'Go to layouts', 'ms-gutenberg-blocks' ) }
										</Button>
										<Button isDestructive onClick={ () => {
											removeBlock();
										} }>
											{ __( 'Remove this block', 'ms-gutenberg-blocks' ) }
										</Button>
									</ButtonGroup>
								</Col>
							}

							{ /* Show the select dropdown when there's more than one category. */ }
							{ categoryOptions.length > 1 &&
								<Col sm={ 12 } style={ { paddingBottom: '1rem' } }>
									<SelectControl
										key={ `layout-library-select-categories-${ clientId }` }
										label={ __( 'Layout Categories', 'ms-gutenberg-blocks' ) }
										value={ activeCategory }
										options={ categoryOptions }
										onChange={ value => setActiveCategory( value ) }
									/>
								</Col>
							}

							{ /* Show the amount of layouts available. */ }
							{ renderedLayouts.length > 0 &&
								<Col sm={ 12 } style={ { paddingBottom: '1rem' } }>
									{ __( 'Amount of available layouts:', 'ms-gutenberg-blocks' ) } { renderedLayouts.length }
								</Col>
							}

							{ /* Display the available layouts. */ }
							{ renderedLayouts.length > 0 && renderedLayouts }
						</Row>
					</Modal>
				) }
			</Placeholder>
		</Fragment>
	);
};

export default compose(
	withSelect( ( select ) => {
		const { canUserUseUnfilteredHTML } = select( 'core/editor' );
		const { getEntityRecords } = select( 'core' );

		return {
			canUserUseUnfilteredHTML,
			layoutPosts: getEntityRecords( 'postType', 'layouts', {
				per_page: -1,
				orderby: 'title',
				order: 'asc',
			} ),
		};
	} ),
	withDispatch( ( dispatch, { canUserUseUnfilteredHTML, clientId } ) => {
		const { removeBlock, replaceBlocks } = dispatch( 'core/block-editor' );
		return ( {
			replaceBlocks: ( blockLayout ) => replaceBlocks(
				clientId,
				rawHandler( {
					HTML: blockLayout,
					mode: 'BLOCKS',
					canUserUseUnfilteredHTML,
				} ),
			),
			removeBlock: () => removeBlock(
				clientId,
				true,
			),
		} );
	} ),
)( edit );
