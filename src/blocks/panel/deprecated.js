/**
 * BLOCK: MS Gutenberg Blocks - Panel
 */
const { InnerBlocks } = wp.blockEditor;
const { Fragment } = wp.element;

const deprecatedAttributes3_0 = {
	index: {
		type: 'number',
		default: 0,
	},
	isActive: {
		type: 'boolean',
		default: true,
	},
	// Padding
	topPadding: {
		type: 'number',
		default: 1,
	},
	bottomPadding: {
		type: 'number',
		default: 1,
	},
	leftPadding: {
		type: 'number',
		default: 1,
	},
	rightPadding: {
		type: 'number',
		default: 1,
	},
};

const deprecatedSave3_0 = ({ attributes }) => {
	const {
		index,
		isActive,
		topPadding,
		leftPadding,
		rightPadding,
		bottomPadding,
	} = attributes;

	const styles = {
		paddingTop: topPadding + 'rem',
		paddingBottom: bottomPadding + 'rem',
		paddingLeft: leftPadding + 'rem',
		paddingRight: rightPadding + 'rem',
	};

	return (
		<Fragment>
			<div
				id={ `panel${ index }` }
				className={ `tabs-panel${ isActive ? ' is-active' : '' }` }
				style={ styles }
			>
				<InnerBlocks.Content />
			</div>
		</Fragment>
	);
};

const deprecated = [
	{
		attributes: deprecatedAttributes3_0,
		save: deprecatedSave3_0,
	},
];

export default deprecated;
