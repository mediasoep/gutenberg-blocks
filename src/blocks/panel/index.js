/**
 * BLOCK: MS Gutenberg Blocks - Panel
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { InnerBlocks, InspectorControls } = wp.blockEditor;
const { registerBlockType } = wp.blocks;
const { PanelBody, Path, RangeControl, SVG } = wp.components;
const { Fragment } = wp.element;

/**
 * Internal dependencies
 */
import deprecated from './deprecated';

//  Import CSS.
import './style.scss';
import './editor.scss';

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'ms/panel', {
	title: __( 'Panel', 'ms-gutenberg-blocks' ),
	parent: [ 'ms/responsive-accordion-tabs' ],
	icon: ( <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" width="24" height="24">
		<Path fill="#f29f95" d="M464 128H272l-64-64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V176c0-26.51-21.49-48-48-48z" />
	</SVG>	),
	category: 'common',
	keywords: [ __( 'Panel', 'ms-gutenberg-blocks' ), 'mediasoep' ],
	supports: {
		inserter: false,
		reusable: false,
		html: false,
	},
	attributes: {
		index: {
			type: 'number',
			default: 0,
		},
		isActive: {
			type: 'boolean',
			default: true,
		},
		// Padding
		topPadding: {
			type: 'number',
			default: 1,
		},
		bottomPadding: {
			type: 'number',
			default: 1,
		},
		leftPadding: {
			type: 'number',
			default: 1,
		},
		rightPadding: {
			type: 'number',
			default: 1,
		},
		// Tablet and mobile scaling factor
		tabletScalingFactor: {
			type: 'number',
			default: 100,
		},
		mobileScalingFactor: {
			type: 'number',
			default: 50,
		},
	},
	edit( { attributes, setAttributes } ) {
		const {
			index,
			isActive,
			topPadding,
			leftPadding,
			rightPadding,
			bottomPadding,
			tabletScalingFactor,
			mobileScalingFactor,
		} = attributes;

		const styles = {
			display: isActive ? 'block' : 'none',
		};

		return (
			<Fragment>
				<InspectorControls>
					<PanelBody
						title={ __( 'Padding', 'ms-gutenberg-blocks' ) }
						initialOpen={ true }
					>
						<h2>{ __( 'Padding (rem)', 'ms-gutenberg-blocks' ) }</h2>
						<RangeControl
							label={ __( 'Top', 'ms-gutenberg-blocks' ) }
							value={ topPadding }
							onChange={ value => {
								setAttributes( {
									topPadding: value,
								} );
							} }
							min={ 0 }
							max={ 20 }
						/>
						<RangeControl
							label={ __( 'Bottom', 'ms-gutenberg-blocks' ) }
							value={ bottomPadding }
							onChange={ value => {
								setAttributes( {
									bottomPadding: value,
								} );
							} }
							min={ 0 }
							max={ 20 }
						/>
						<RangeControl
							label={ __( 'Left', 'ms-gutenberg-blocks' ) }
							value={ leftPadding }
							onChange={ value => {
								setAttributes( {
									leftPadding: value,
								} );
							} }
							min={ 0 }
							max={ 20 }
						/>
						<RangeControl
							label={ __( 'Right', 'ms-gutenberg-blocks' ) }
							value={ rightPadding }
							onChange={ value => {
								setAttributes( {
									rightPadding: value,
								} );
							} }
							min={ 0 }
							max={ 20 }
						/>
						<h2>{ __( 'Scaling on tablet and mobile', 'ms-gutenberg-blocks' ) }</h2>
						<p>
							<i>
								{ __(
									'Percentage based on the values selected in margin and padding.',
									'ms-gutenberg-blocks'
								) }
							</i>
						</p>
						<RangeControl
							label={ __( 'Tablet scaling factor (%)', 'ms-gutenberg-blocks' ) }
							value={ tabletScalingFactor }
							onChange={ value => {
								setAttributes( {
									tabletScalingFactor: value,
								} );
							} }
							min={ 5 }
							max={ 100 }
							step={ 5 }
						/>
						<RangeControl
							label={ __( 'Mobile scaling factor (%)', 'ms-gutenberg-blocks' ) }
							value={ mobileScalingFactor }
							onChange={ value => {
								setAttributes( {
									mobileScalingFactor: value,
								} );
							} }
							min={ 5 }
							max={ 100 }
							step={ 5 }
						/>
					</PanelBody>
				</InspectorControls>
				<div
					id={ `panel${ index }` }
					className={ `tabs-panel${ isActive ? ' is-active' : '' }` }
					style={ styles }
				>
					<InnerBlocks templateLock={ false } renderAppender={
						InnerBlocks.ButtonBlockAppender
					} />
				</div>
			</Fragment>
		);
	},
	save( { attributes } ) {
		const {
			index,
			isActive,
		} = attributes;

		return (
			<Fragment>
				<div
					id={ `panel${ index }` }
					className={ `tabs-panel${ isActive ? ' is-active' : '' }` }
				>
					<InnerBlocks.Content />
				</div>
			</Fragment>
		);
	},
	deprecated,
} );
