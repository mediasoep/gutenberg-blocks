/**
 * BLOCK: MS Gutenberg Blocks - Animate
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Path, SVG } = wp.components;

/**
 * Internal dependencies
 */
import attributes from './attributes';
import edit from './edit';
import save from './save';

//  Import CSS.
import './style.scss';
import './editor.scss';

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'ms/animate', {
	title: __( 'Animate', 'ms-gutenberg-blocks' ),
	icon: ( <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="24" height="24">
		<Path fill="#f29f95" d="M244.5 231.4l-192-160C31.9 54.3 0 68.6 0 96v320c0 27.4 31.9 41.8 52.5 24.6l192-160c15.3-12.8 15.3-36.4 0-49.2z" /><Path d="M500.5 231.4l-192-160C287.9 54.3 256 68.6 256 96v320c0 27.4 31.9 41.8 52.5 24.6l192-160c15.3-12.8 15.3-36.4 0-49.2z" fill="#e84f3d"/>
	</SVG> ),
	category: 'layout',
	description: __( 'Animate blocks inside this container.', 'ms-gutenberg-blocks' ),
	keywords: [
		__( 'Animate', 'ms-gutenberg-blocks' ),
		__( 'Animation', 'ms-gutenberg-blocks' ),
		'mediasoep'
	],
	supports: {
		align: [ 'full' ],
	},
	attributes,
	edit,
	save,
} );
