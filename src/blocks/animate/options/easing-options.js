const { applyFilters } = wp.hooks;
const { __ } = wp.i18n;

let easingOptions = [
	{
		label: 'Ease',
		value: 'ease',
	},
	{
		label: 'Ease in',
		value: 'ease-in',
	},
	{
		label: 'Ease out',
		value: 'ease-out',
	},
	{
		label: 'Ease in out',
		value: 'ease-in-out',
	},
	{
		label: 'Ease in back',
		value: 'ease-in-back',
	},
	{
		label: 'Ease out back',
		value: 'ease-out-back',
	},
	{
		label: 'Ease in out back',
		value: 'ease-in-out-back',
	},
	{
		label: 'Ease in sine',
		value: 'ease-in-sine',
	},
	{
		label: 'Ease out sine',
		value: 'ease-out-sine',
	},
	{
		label: 'Ease in out sine',
		value: 'ease-in-out-sine',
	},
	{
		label: 'Ease in quad',
		value: 'ease-in-quad',
	},
	{
		label: 'Ease out quad',
		value: 'ease-out-quad',
	},
	{
		label: 'Ease in out quad',
		value: 'ease-in-out-quad',
	},
	{
		label: 'Ease in cubic',
		value: 'ease-in-cubic',
	},
	{
		label: 'Ease out cubic',
		value: 'ease-out-cubic',
	},
	{
		label: 'Ease in out cubic',
		value: 'ease-in-out-cubic',
	},
	{
		label: 'Ease in quart',
		value: 'ease-in-quart',
	},
	{
		label: 'Ease out quart',
		value: 'ease-out-quart',
	},
	{
		label: 'Ease in out quart',
		value: 'ease-in-out-quart',
	},
	{
		label: 'Linear',
		value: 'linear',
	},
];

easingOptions = applyFilters( 'animateBlocks.easingOptions', easingOptions );

export default easingOptions;
