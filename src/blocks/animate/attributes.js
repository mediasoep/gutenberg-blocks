/**
 * BLOCK: MS Gutenberg Blocks - Animate
 */

/**
 * Internal dependencies
 */
import anchorPlacementOptions from './options/anchor-placement-options';
import animationOptions from './options/animation-options';
import easingOptions from './options/easing-options';

let defaultOptions = {
	animation: animationOptions && animationOptions.length > 0 ? animationOptions[ 0 ].value : 'fade',
	offset: 120,
	delay: 0,
	duration: 400,
	easing: easingOptions && easingOptions.length > 0 ? easingOptions[ 0 ].value : 'ease',
	once: true,
	mirror: false,
	anchorPlacement: anchorPlacementOptions && anchorPlacementOptions.length > 0 ? anchorPlacementOptions[ 0 ].value : 'top-bottom',
};
defaultOptions = wp.hooks.applyFilters( 'animateBlocks.defaultOptions', defaultOptions );

const attributes = {
	align: {
		type: 'string',
		default: 'full',
	},
	animation: {
		type: 'string',
		default: defaultOptions.animation,
	},
	animationOffset: {
		type: 'number',
		default: defaultOptions.offset,
	},
	animationDelay: {
		type: 'number',
		default: defaultOptions.delay,
	},
	animationDuration: {
		type: 'number',
		default: defaultOptions.duration,
	},
	animationEasing: {
		type: 'string',
		default: defaultOptions.easing,
	},
	animationOnce: {
		type: 'boolean',
		default: defaultOptions.once,
	},
	animationMirror: {
		type: 'boolean',
		default: defaultOptions.mirror,
	},
	animationAnchorPlacement: {
		type: 'string',
		default: defaultOptions.anchorPlacement,
	},
};

export default attributes;
