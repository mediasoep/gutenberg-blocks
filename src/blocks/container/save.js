/**
 * BLOCK: MS Gutenberg Blocks - Container
 */

/**
 * WordPress dependencies
 */
const { InnerBlocks, useBlockProps } = wp.blockEditor;

/**
 * Internal dependencies
 */
import getAppearance from './appearance';

const save = ( props ) => {
	const {
		attributes: {
			uniqueID,
			backgroundType,
			backgroundImageUrl,
			backgroundImageHeight,
			backgroundImageWidth,
			backgroundImageParallax,
			backgroundImageParallaxSpeed,
		},
	} = props;
	const { classes, wrapperClasses, inlineStyles, internalStyles } = getAppearance( props );

	const parallax = backgroundImageParallax && backgroundImageUrl ?
		{
			'data-parallax': 'scroll',
			'data-image-src': backgroundImageUrl,
			'data-z-index': 1,
			'data-natural-width': backgroundImageWidth,
			'data-natural-height': backgroundImageHeight,
			'data-speed': backgroundImageParallaxSpeed * 0.1,
		} :
		null;

	return (
		<div
			{ ...useBlockProps.save( {
				id: `ms-container-${ uniqueID }`,
				className: classes,
				style: inlineStyles,
				...parallax,
			} ) }
		>
			{ backgroundType === 'video' && (
				<video
					className="ms-video-background"
					autoPlay
					muted
					loop
					src={ backgroundImageUrl }
				/>
			) }
			<div className={ wrapperClasses }>
				<InnerBlocks.Content />
			</div>
			{ internalStyles && (
				<style>
					{ internalStyles }
				</style>
			) }
		</div>
	);
};

export default save;
