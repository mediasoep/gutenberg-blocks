/**
 * BLOCK: MS Gutenberg Blocks - Container
 */
const attributes = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	align: {
		type: 'string',
		default: 'full',
	},
	// Size settings
	height: {
		type: 'string',
		default: 'normal',
	},
	contentWidth: {
		type: 'string',
		default: 'normal',
	},
	// Background settings
	backgroundType: {
		type: 'string',
		default: '',
	},
	backgroundOpacity: {
		type: 'number',
		default: 5,
	},
	// Background color
	backgroundColorType: {
		type: 'string',
		default: '',
	},
	backgroundColor: {
		type: 'string',
		default: '',
	},
	backgroundColor2: {
		type: 'string',
		default: '',
	},
	backgroundColorDirection: {
		type: 'number',
		default: 0,
	},
	// Background image
	backgroundImageID: {
		type: 'number',
	},
	backgroundImageUrl: {
		type: 'string',
	},
	backgroundImageHeight: {
		type: 'number',
	},
	backgroundImageWidth: {
		type: 'number',
	},
	backgroundImageColor: {
		type: 'string',
		default: '',
	},
	backgroundFocalPoint: {
		type: 'object',
		default: {
			x: 0.5,
			y: 0.5,
		},
	},
	backgroundImageAttachment: {
		type: 'boolean',
		default: false,
	},
	backgroundImageParallax: {
		type: 'boolean',
		default: false,
	},
	backgroundImageParallaxSpeed: {
		type: 'number',
		default: 2,
	},
	isDark: {
		type: 'boolean',
		default: false,
	},
	// Padding
	topPadding: {
		type: 'number',
		default: 0,
	},
	bottomPadding: {
		type: 'number',
		default: 0,
	},
	leftPadding: {
		type: 'number',
		default: 0,
	},
	rightPadding: {
		type: 'number',
		default: 0,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
	// Tablet and mobile scaling factor
	tabletScalingFactor: {
		type: 'number',
		default: 100,
	},
	mobileScalingFactor: {
		type: 'number',
		default: 50,
	},
};

export default attributes;
