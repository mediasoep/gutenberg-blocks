/**
 * WordPress dependencies
 */
const { registerFormatType } = wp.richText;

/**
 * Internal dependencies
 */
import formats from './formats';

formats.forEach( ( { name, ...settings } ) =>
	registerFormatType( name, settings )
);