/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { toggleFormat } = wp.richText;
const { RichTextToolbarButton } = wp.blockEditor;
const { Path, SVG } = wp.components;

/**
 * Format constants
 */
const name = 'wpsoup/no-translate';

export const noTranslate = {
	name,
	title: __( 'No translate', 'ms-gutenberg-blocks' ),
	tagName: 'span',
	className: 'no-translate',
	attributes: {
		'data-wg-notranslate': '',
	},
	edit( { isActive, value, onChange } ) {
		const onToggle = () => {
			onChange(
				toggleFormat( value, {
					type: name,
					attributes: {
						'data-wg-notranslate': '',
					},
				} )
			);
		};
		return (
			<Fragment>
				<RichTextToolbarButton
					icon="translation"
					title={ __( 'No translate', 'ms-gutenberg-blocks' ) }
					onClick={ onToggle }
					isActive={ isActive }
				/>
			</Fragment>
		);
	},
};
