// /**
//  * WordPress dependencies
//  */
// const { __ } = wp.i18n;
// const { toggleFormat } = wp.richText;
// const { RichTextToolbarButton } = wp.blockEditor;

// const name = 'wpsoup/tooltip';
// const title = __('Tooltip');

// export const tooltip = {
// 	name,
// 	title,
// 	tagName: 's',
// 	className: null,
// 	edit( { isActive, value, onChange, onFocus } ) {
// 		function onClick() {
// 			onChange( toggleFormat( value, { type: name } ) );
// 			onFocus();
// 		}

// 		return (
// 			<RichTextToolbarButton
// 				icon="editor-strikethrough"
// 				title={ title }
// 				onClick={ onClick }
// 				isActive={ isActive }
// 			/>
// 		);
// 	},
// };

// https://github.com/phpbits/block-options/blob/master/src/extensions/formats/text-color/components/edit.js
// https://github.com/gambitph/Stackable/blob/master/src/format-types/highlight/index.js
// https://gu10.blog/2019/11/04/comparison-adding-inline-text-styles/
// https://github.com/WordPress/gutenberg/blob/4857ad58c1241b3d63d21a6880c989b85746c3dc/packages/format-library/src/link/inline.js
// https://developer.wordpress.org/block-editor/tutorials/format-api/1-register-format/