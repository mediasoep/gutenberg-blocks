<?php

/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
// Hook: Frontend assets.
add_action('enqueue_block_assets', 'ms_blocks_cgb_block_assets');
function ms_blocks_cgb_block_assets()
{ // phpcs:ignore
	// Styles.
	wp_enqueue_style(
		'ms_blocks-cgb-style-css', // Handle.
		plugins_url('dist/blocks.style.build.css', dirname(__FILE__)), // Block style CSS.
		is_admin() ? array('wp-editor') : null // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
	);
}

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
// Hook: Editor assets.
add_action('enqueue_block_editor_assets', 'ms_blocks_cgb_editor_assets');
function ms_blocks_cgb_editor_assets()
{ // phpcs:ignore
	// Scripts.
	wp_enqueue_script(
		'ms-gutenberg-blocks-js', // Handle.
		plugins_url('/dist/blocks.build.js', dirname(__FILE__)), // Block.build.js: We register the block here. Built with Webpack.
		array('wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor'), // Dependencies, defined above.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: File modification time.
		true // Enqueue the script in the footer.
	);

	wp_localize_script(
		'ms-gutenberg-blocks-js', // Handle.
		'localizedBlocks',
		[
			'layoutsPostTypePage' => admin_url('edit.php?post_type=layouts')
		]
	);

	// Styles.
	wp_enqueue_style(
		'ms-gutenberg-blocks-css', // Handle.
		plugins_url('dist/blocks.editor.build.css', dirname(__FILE__)), // Block editor CSS.
		array('wp-edit-blocks') // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
	);

	// Translations.
	wp_set_script_translations('ms-gutenberg-blocks-js', 'ms-gutenberg-blocks', plugin_dir_path(__DIR__) . 'languages');
}

add_action('rest_api_init', 'register_category_names_field');
function register_category_names_field()
{
	register_rest_field(
		'layouts',
		'category_names',
		array(
			'get_callback'    => 'get_category_names',
			'schema'          => null,
		)
	);
}

function get_category_names($object)
{
	$post_id = $object['id'];
	$terms = get_the_terms($post_id, 'layout_category');
	if (is_array($terms)) {
		$termNames = [];
		foreach ($terms as $term) {
			$termNames[] = $term->name;
		}

		return $termNames;
	}

	return [];
}
