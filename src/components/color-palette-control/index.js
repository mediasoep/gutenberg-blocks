/**
 * Color Palette Control
 *
 * We need to implement our own until this is resolved:
 * https://github.com/WordPress/gutenberg/issues/13018
 *
 * Credits: https://github.com/gambitph/Stackable/blob/acbb5c14ad8acb38e22c29a53e966eb8d40f050a/src/components/color-palette-control/index.js
 */
const { __, sprintf } = wp.i18n;
const { BaseControl, ColorIndicator, ColorPalette } = wp.components;
const { compose, ifCondition } = wp.compose;
const { getColorObjectByColorValue, withColorContext } = wp.blockEditor;
const { Fragment } = wp.element;

// translators: first %s: The type of color (e.g. background color), second %s: the color name or value (e.g. red or #ff0000)
const colorIndicatorAriaLabel = __( '(current %s: %s)', 'ms-gutenberg-blocks' );

const ColorPaletteControl = ( {
	colors,
	disableCustomColors,
	label,
	onChange,
	value,
} ) => {
	const colorObject = getColorObjectByColorValue( colors, value );
	const colorName = colorObject && colorObject.name;
	const ariaLabel = sprintf( colorIndicatorAriaLabel, label.toLowerCase(), colorName || value );

	const labelElement = (
		<Fragment>
			{ label }
			{ value && (
				<ColorIndicator
					colorValue={ value }
					aria-label={ ariaLabel }
				/>
			) }
		</Fragment>
	);

	return (
		<BaseControl
			className="editor-color-palette-control"
			label={ labelElement }>
			<ColorPalette
				className="editor-color-palette-control__color-palette"
				value={ value }
				onChange={ onChange }
				{ ... { colors, disableCustomColors } }
			/>
		</BaseControl>
	);
};

export default compose( [
	withColorContext,
	ifCondition( ( { hasColorsToChoose } ) => hasColorsToChoose ),
] )( ColorPaletteControl );
